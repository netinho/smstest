package com.chichemeinc.mobile.smstest.util;

import android.app.Activity;
import android.net.ConnectivityManager;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Francisco Ernesto Teixeira on 02/07/15.
 */
public class NetworkUtil {

    public static boolean verifyConnectivity(Activity activity) {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(activity.getBaseContext().CONNECTIVITY_SERVICE);

        if (isConnected(connectivityManager)) {
            Toast.makeText(activity, " Conectado ", Toast.LENGTH_LONG).show();

            return true;
        } else if (isNotConnected(connectivityManager)) {
            Toast.makeText(activity, " Não Conectado ", Toast.LENGTH_LONG).show();

            return false;
        }

        return false;
    }

    private static boolean isConnected(ConnectivityManager connectivityManager) {
        return connectivityManager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connectivityManager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connectivityManager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED;
    }

    private static boolean isNotConnected(ConnectivityManager connectivityManager) {
        return connectivityManager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                connectivityManager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED;
    }

    public static InputStream openHttpConnection(String urlStr) throws MalformedURLException, IOException {
        InputStream in = null;
        int resCode;

        URL url = new URL(urlStr);
        URLConnection urlConn = url.openConnection();

        if (!(urlConn instanceof HttpURLConnection)) {
            throw new IOException("URL is not an Http URL");
        }
        HttpURLConnection httpConn = (HttpURLConnection) urlConn;
        httpConn.setAllowUserInteraction(false);
        httpConn.setInstanceFollowRedirects(true);
        httpConn.setRequestMethod("GET");
        httpConn.connect();
        resCode = httpConn.getResponseCode();

        if (resCode == HttpURLConnection.HTTP_OK) {
            in = httpConn.getInputStream();
        }

        return in;
    }

}
