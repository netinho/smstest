//
//  ViewController.swift
//  SmsTest
//
//  Created by Francisco Ernesto Teixeira on 01/07/15.
//  Copyright (c) 2015 Francisco Ernesto Teixeira. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    
    @IBOutlet weak var fieldTelefone: UITextField!
    @IBOutlet weak var buttonSolicitarCodigo: UIButton!
    @IBOutlet weak var fieldCodigo: UITextField!
    @IBOutlet weak var buttonEnviarCodigo: UIButton!
    @IBOutlet weak var labelRetornoIcone: UILabel!
    @IBOutlet weak var labelRetorno: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func solicitarCodigo(segue:UIStoryboardSegue) {
        println(fieldTelefone.text)
        println(UIDevice.currentDevice().identifierForVendor.UUIDString)
        buttonSolicitarCodigo.enabled = false
        /*
        let authorizationToken = "Token teste"
        Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = ["Authorization": authorizationToken]
        Alamofire.request(.GET, "http://httpbin.org/get", parameters: ["foo": "bar"])
        .response { (request, response, data, error) in
        println(request)
        println(response)
        println(data)
        println(error)
        }
        */
    }
    
    @IBAction func enviarCodigo(segue: UIStoryboardSegue) {
        
        let valido = false
        
        if (valido) {
            labelRetornoIcone.text = "\u{f058}"
            labelRetornoIcone.textColor = UIColor(hexColorCode: "#274E13")
            labelRetorno.text = "Código válido"
        } else {
            labelRetornoIcone.text = "\u{f057}"
            labelRetornoIcone.textColor = UIColor(hexColorCode: "#660000")
            labelRetorno.text = "Código inválido"
        }
    }
    
}

